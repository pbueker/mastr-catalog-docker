FROM python:3


RUN \
  apt-get update && \
  apt-get install -y spatialite-bin libsqlite3-mod-spatialite \
     binutils libproj-dev gdal-bin \
     wget
     
RUN apt-get -y autoremove
RUN apt-get -y autoclean
RUN apt-get -y clean

RUN pip install flask flask-jsonpify flask-sqlalchemy flask-restful webargs spatialite geoalchemy2 marshmallow flask-marshmallow marshmallow-sqlalchemy shapely shapely_geojson

RUN wget https://gitlab.com/pbueker/mastr-catalog-docker/-/raw/master/my_script.py

RUN mkdir -p /data

WORKDIR /data

VOLUME ["/data"]

#ADD my_script.py /


#CMD [ "python", "./my_script.py" ]

EXPOSE 5002
ENTRYPOINT ["python", "/my_script.py"]
