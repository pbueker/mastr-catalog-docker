import logging

from flask import Flask, request
from flask_restful import Resource, Api
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.event import listen
from sqlalchemy.sql import table, column, select, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import types
from sqlalchemy import Column, Integer, String, Numeric
from geoalchemy2 import Geometry, shape, elements
from geoalchemy2.shape import from_shape, to_shape
from sqlalchemy import Column, Integer, String, Boolean, Float, Text, DateTime, Date
from datetime import date
from pprint import pprint

from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields, post_load, post_dump, INCLUDE, ValidationError

import shapely
from shapely import geometry
from shapely.wkt import dumps, loads

from shapely_geojson import dumps, Feature

from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort

from json import dumps
from flask_jsonpify import jsonify

from sqlalchemy.orm import sessionmaker

def load_spatialite(dbapi_conn, connection_record):
    dbapi_conn.enable_load_extension(True)
    dbapi_conn.load_extension('mod_spatialite.so')


db_connect = create_engine('sqlite:////data/mastr.db', echo=True)
listen(db_connect, 'connect', load_spatialite)
db_connect.execute(select([func.InitSpatialMetaData(1)]))
Base = declarative_base()
Session = sessionmaker(bind=db_connect)

class unit(Base):
    __tablename__ = 'unit'
    MaStRNummer = Column(String, primary_key=True)
    Id = Column(Integer)
    EinheitName = Column(String)
    BetriebsStatusName = Column(String)
    BetriebsStatusId = Column(Integer)
    EnergietraegerName = Column(String)
    Nettonennleistung = Column(Integer)
    Bruttoleistung = Column(Integer)
    Nabenhoehe = Column(Numeric)
    Rotordurchmesser = Column(Numeric)
    #created = Column(DateTime, default=datetime.now())
    geom = Column(Geometry(geometry_type='POINT', srid=4326, management=True))
    GenehmigungsMastr = Column(String)
    created = Column(DateTime, default=datetime.now())
    updated = Column(DateTime, default=datetime.now())

    def __repr__(self):
        return '<unit(%s)>' % (self.MaStRNummer)
    

class owner(Base):
    __tablename__ = 'owner'
    MaStRNummer = Column(String, primary_key=True)
    Id = Column(Integer)
    Name = Column(String, nullable=False)
    Strasse = Column(String)
    Plz = Column(String)
    Ort = Column(String)
    Marktfunktion = Column(String)
    Rechtsform = Column(String)
    Registergericht = Column(String)
    Registernummer = Column(String)
    Kmu = Column(String)
    TaetigkeitBegin = Column(Date)
    TaetigkeitEnd = Column(Date)
    created = Column(DateTime, default=datetime.now())
    updated = Column(DateTime, default=datetime.now())
    #geom = Column(Geometry('POINT'))
    

class genehmigung(Base):
    __tablename__ = 'genehmigung'
    MaStRNummer = Column(String, primary_key=True)
    Id = Column(Integer)
    Meldedatum = Column(Date)
    Aktualisierung = Column(Date)
    Art =  Column(String)
    Genehmigungsdatum = Column(Date)
    Genehmigungsbehoerde =  Column(String)
    GenehmigungsAZ = Column(String)
    Errichtungsfrist = Column(Date)
    created = Column(DateTime, default=datetime.now())
    updated = Column(DateTime, default=datetime.now())
    

Base.metadata.create_all(db_connect)

app = Flask(__name__)
app.logger.setLevel(logging.DEBUG)
ma = Marshmallow(app)


class UnitSchema(ma.Schema):
    __envelope__ = {"single": "unit", "many": "units"}
    __geometry_field_name__ = "geom"
    class Meta:
        fields = ('MaStRNummer', 'Id', 'EinheitName', 'BetriebsStatusName', 'BetriebsStatusId', 'Nabenhoehe', 'Rotordurchmesser', 'Nettonennleistung', 'Bruttoleistung', 'geom', 'lon', 'lat', 'EnergietraegerName', 'Genehmigung')
        unknown = INCLUDE
    def get_envelope_key(self, many):
        """Helper to get the envelope key."""
        key = self.__envelope__["many"] if many else self.__envelope__["single"]
        assert key is not None, "Envelope key undefined"
        return key
    def wrap_feature(self, data):
        """
        Wrap the individual feature as a GeoJSON feature object
        """
        feature = {
            'type': 'Feature',
            'geometry': shapely.geometry.mapping(to_shape(data.pop(self.__geometry_field_name__)))
        }
        feature['properties'] = data
        return feature
    @post_load
    def make_unit(self, data, **kwargs):
        return unit(**data)
    @post_dump(pass_many=True)
    def wrap_with_envelope(self, data, many, **kwargs):
        """
        Wrap the individual feature as a GeoJSON feature object
        """
        if not many:
            return self.wrap_feature(data)
        
        features = {
            'type': 'FeatureCollection',
            'features' : [self.wrap_feature(feature) for feature in data]
        }
        #feature['properties'] = data
        #key = self.get_envelope_key(many)
        #return {key: data}
        return features
    
    
class GenehmigungSchema(ma.Schema):
    __envelope__ = {"single": "Genhmigung", "many": "Genehmigungen"}
    class Meta:
        fields = ('MaStRNummer', 'Id', 'Meldedatum', 'Aktualisierung', 'Art', 'Genehmigungsdatum', 'Genehmigungsbehoerde', 'GenehmigungsAZ', 'Errichtungsfrist')
        unknown = INCLUDE
    def get_envelope_key(self, many):
        """Helper to get the envelope key."""
        key = self.__envelope__["many"] if many else self.__envelope__["single"]
        assert key is not None, "Envelope key undefined"
        return key
    @post_load
    def make_Genehmigung(self, data, **kwargs):
        return genehmigung(**data)
    

@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code



api = Api(app)


class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

class testSpatialite(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute(select([func.InitSpatialMetaData()]))
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)
    
class getSingleUnit(Resource):
    unit_args = {"MaStRNummer": fields.Str(required=True)}

    @use_kwargs(unit_args,location="query")
    def get(self, MaStRNummer):
        session = Session()
        result = session.query(unit).order_by(unit.Id)
        result1 = UnitSchema(many=True).dump(result.all())
        return jsonify (result1 )

class addSingleUnit(Resource):
    unit_args = {"MaStRNummer": fields.Str(required=True),
                 "Id" : fields.Integer(),
                 "EinheitName" : fields.Str(required=True),
                 "BetriebsStatusName" : fields.Str(),
                 "BetriebsStatusId" : fields.Integer(),
                 'Bruttoleistung' : fields.Integer(),
                 'Nettonennleistung' : fields.Integer(),
                 'EnergietraegerName' : fields.Str(),
                 'geom' : fields.Str(),
                 'lon' : fields.Decimal(),
                 'lat' : fields.Decimal() }
    
    @use_args(unit_args,location="query")
    def get(self, args):
        #conn = db_connect.connect()
        session = Session()
        #dump(args)
        args['geom'] = shape.from_shape(shapely.wkt.loads("POINT(" + str(args['lon']) + ' ' + str(args['lat']) + ")"), srid=4326)
        del args['lon']
        del args['lat']
        my_unit = UnitSchema().load(args)
        session.add(my_unit)
        print(session.dirty)
        session.commit()
    
    

class addSingleGenehmigung(Resource):
    gen_args = {"MaStRNummer": fields.Str(required=True),
                 "Id" : fields.Integer(),
                 "Meldedatum" : fields.Date(format='iso'),
                 "Aktualisierung" : fields.Date(format='iso'),
                 "Art" : fields.Str(),
                 'Genehmigungsdatum' : fields.Date(format='iso'),
                 'Genehmigungsbehoerde' : fields.Str(),
                 'GenehmigungsAZ' : fields.Str(),
                 'Errichtungsfrist' : fields.Date(format='iso')}

    @use_args(gen_args,location="query")
    def get(self, args):
        #conn = db_connect.connect()
        session = Session()
        #dump(args)
        my_Genehm = GenehmigungSchema().load(args)
        session.add(my_Genehm)
        print(session.dirty)
        session.commit()
        
class getSingleGenehmigung(Resource):
    unit_args = {"MaStRNummer": fields.Str(required=True)}

    @use_kwargs(unit_args,location="query")
    def get(self, MaStRNummer):
        session = Session()
        result = session.query(genehmigung).order_by(genehmigung.Id)
        
        result1 = GenehmigungSchema(many=True).dump(result.all())
        
        return jsonify (result1 )


api.add_resource(HelloWorld, '/')

api.add_resource(testSpatialite, '/test')

api.add_resource(getSingleUnit, '/getSingleUnit')
api.add_resource(getSingleGenehmigung, '/getSingleGenehmigung')

api.add_resource(addSingleUnit, '/addSingleUnit')

api.add_resource(addSingleGenehmigung, '/addSingleGenehmigung')



if __name__ == '__main__':
     app.run(host='0.0.0.0', port='5002', debug=True)

